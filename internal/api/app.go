package api

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/dvkgroup/go-library/internal/controller"
	"gitlab.com/dvkgroup/go-library/internal/repository/sqlDB"
	"gitlab.com/dvkgroup/go-library/internal/usercase"
)

func Run(config map[string]string, db *sqlx.DB) {
	r := sqlDB.NewLibraryStorage(db)
	u := usercase.NewHLibraryUseCase(r)
	c := controller.NewLibraryController(u)

	_ = c
	c.Run(config)
}
