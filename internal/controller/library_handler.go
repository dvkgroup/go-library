package controller

import (
	"encoding/json"
	"gitlab.com/dvkgroup/go-library/internal/entity"
	"net/http"
	"strconv"
)

type LibraryUseCase interface {
	AddUser(u entity.User) (entity.User, error)
	ListUsers() []entity.UserResponse
	AddAuthor(a entity.Author) (entity.Author, error)
	ListAuthors() []entity.AuthorResponse
	TopAuthors(count int) []entity.AuthorTopResponse
	AddBook(b entity.Book) (entity.Book, error)
	ListBooks() []entity.BookResponse
	GetBook(bookId int, userId int) (entity.Book, error)
	ReturnBook(bookId int, userId int) (entity.Book, error)
}

type LibraryController struct {
	u LibraryUseCase
}

func NewLibraryController(u LibraryUseCase) *LibraryController {
	return &LibraryController{u: u}
}

// Users
func (c LibraryController) AddUser(w http.ResponseWriter, r *http.Request) {
	userName := r.FormValue("UserName")
	if userName == "" {
		http.Error(w, "Empty user name", http.StatusBadRequest)
		return
	}

	user := entity.User{Name: userName}

	user, err := c.u.AddUser(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	writeResponse(w, user)
}

func (c LibraryController) GetListUsers(w http.ResponseWriter, r *http.Request) {
	users := c.u.ListUsers()
	writeResponse(w, users)
}

// Authors
func (c LibraryController) AddAuthor(w http.ResponseWriter, r *http.Request) {
	authorName := r.FormValue("AuthorName")
	if authorName == "" {
		http.Error(w, "Empty author name", http.StatusBadRequest)
		return
	}

	author := entity.Author{Name: authorName}

	author, err := c.u.AddAuthor(author)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	writeResponse(w, author)
}

func (c LibraryController) GetListAuthors(w http.ResponseWriter, r *http.Request) {
	authors := c.u.ListAuthors()
	writeResponse(w, authors)
}

func (c LibraryController) GetTopAuthors(w http.ResponseWriter, r *http.Request) {
	count, err := strconv.Atoi(r.FormValue("count"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	authors := c.u.TopAuthors(count)
	writeResponse(w, authors)
}

// Books
func (c LibraryController) AddBook(w http.ResponseWriter, r *http.Request) {
	title := r.FormValue("title")
	if title == "" {
		http.Error(w, "Empty title", http.StatusBadRequest)
		return
	}

	authorId, err := strconv.Atoi(r.FormValue("author_id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	book := entity.Book{Title: title, AuthorId: authorId}

	book, err = c.u.AddBook(book)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	writeResponse(w, book)
}

func (c LibraryController) GetBook(w http.ResponseWriter, r *http.Request) {
	bookId, err := strconv.Atoi(r.FormValue("book_id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	userId, err := strconv.Atoi(r.FormValue("user_id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	book, err := c.u.GetBook(bookId, userId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	writeResponse(w, book)
}

func (c LibraryController) ReturnBook(w http.ResponseWriter, r *http.Request) {
	bookId, err := strconv.Atoi(r.FormValue("book_id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	userId, err := strconv.Atoi(r.FormValue("user_id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	book, err := c.u.ReturnBook(bookId, userId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	writeResponse(w, book)
}

func (c LibraryController) GetListBooks(w http.ResponseWriter, r *http.Request) {
	books := c.u.ListBooks()
	writeResponse(w, books)
}

func writeResponse(w http.ResponseWriter, data interface{}) {
	// выставляем заголовки, что отправляем json в utf8
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err := json.NewEncoder(w).Encode(data)

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
