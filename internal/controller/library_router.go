package controller

import (
	"net/http"

	"github.com/go-chi/chi"
)

func libraryRouter(c *LibraryController) http.Handler {
	r := chi.NewRouter()

	r.Post("/user/add", c.AddUser)
	r.Get("/user/list", c.GetListUsers)
	r.Post("/author/add", c.AddAuthor)
	r.Get("/author/list", c.GetListAuthors)
	r.Get("/author/top", c.GetTopAuthors)
	r.Post("/book/add", c.AddBook)
	r.Get("/book/list", c.GetListBooks)
	r.Post("/book/get", c.GetBook)
	r.Post("/book/return", c.ReturnBook)

	return r
}
