package entity

type Book struct {
	Id       int    `db:"id"`
	Title    string `db:"title"`
	AuthorId int    `db:"author_id"`
}

type BookResponse struct {
	Book
	Author Author
}
