package entity

type Author struct {
	Id   int    `bd:"id"`
	Name string `bd:"name"`
}

type AuthorResponse struct {
	Author
	Books []Book
}

type AuthorTopResponse struct {
	Author
	Rating int `bd:"rating"`
}
