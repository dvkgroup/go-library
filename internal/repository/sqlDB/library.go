package sqlDB

import (
	"database/sql"
	"errors"
	"fmt"
	"github.com/brianvoe/gofakeit/v6"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/dvkgroup/go-library/internal/entity"
	"log"
	"math/rand"
	"time"
)

type LibraryStorage struct {
	db *sqlx.DB
}

func NewLibraryStorage(db *sqlx.DB) *LibraryStorage {
	s := &LibraryStorage{db: db}

	s.checkDB()

	return s
}

func (s LibraryStorage) checkDB() {
	s.checkAuthors()
	s.checkBooks()
	s.checkUsers()
}

func (s LibraryStorage) checkAuthors() {
	if authors := s.ListAuthors(); authors != nil {
		return
	}

	for i := 0; i < 10; i++ {
		a := entity.Author{
			Name: gofakeit.Name(),
		}

		s.AddAuthor(a)
	}
}

func (s LibraryStorage) checkBooks() {
	if books := s.ListBooks(); books != nil {
		return
	}

	authors := s.ListAuthors()

	seed := rand.NewSource(time.Now().UnixNano())
	rnd := rand.New(seed)

	for i := 0; i < 100; i++ {
		r1 := rnd.Intn(len(authors))
		r2 := rnd.Intn(3) + 1 // количество слов в названии книги 1-3

		b := entity.Book{
			AuthorId: authors[r1].Id,
			Title:    gofakeit.Sentence(r2),
		}

		s.AddBook(b)
	}
}

func (s LibraryStorage) checkUsers() {
	if users := s.ListUsers(); users != nil {
		return
	}

	for i := 0; i < 51; i++ {
		u := entity.User{
			Name: gofakeit.Name(),
		}

		s.AddUser(u)
	}
}

func (s LibraryStorage) AddUser(u entity.User) (entity.User, error) {
	var id int64

	query := `INSERT INTO users (name) VALUES ($1) RETURNING id`

	err := s.db.QueryRow(query, u.Name).Scan(&id)
	if err != nil {
		return entity.User{}, err
	}

	u.Id = int(id)

	return u, nil
}

func (s LibraryStorage) ListUsers() []entity.UserResponse {
	var res []entity.UserResponse

	query := `SELECT * FROM users`

	users, err := s.db.Queryx(query)
	if err != nil {
		log.Println(err)
		return nil
	}

	var ur entity.UserResponse
	var u entity.User
	for users.Next() {
		err := users.StructScan(&u)
		if err != nil {
			log.Println(err)
			return nil
		}

		ur.User = u

		var books []entity.Book
		query2 := `SELECT id, title, author_id FROM books WHERE user_id=$1`

		err = s.db.Select(&books, query2, u.Id)
		if err != nil {
			log.Println(err)
			return nil
		}
		ur.RentedBooks = books

		res = append(res, ur)
	}

	return res
}

func (s LibraryStorage) AddAuthor(a entity.Author) (entity.Author, error) {
	var id int64
	query := `INSERT INTO authors (name) VALUES ($1) RETURNING id`

	err := s.db.QueryRow(query, a.Name).Scan(&id)
	if err != nil {
		return entity.Author{}, err
	}

	a.Id = int(id)

	return a, nil
}

func (s LibraryStorage) ListAuthors() []entity.AuthorResponse {
	var res []entity.AuthorResponse

	query := `SELECT id, name FROM authors`

	authors, err := s.db.Queryx(query)
	if err != nil {
		log.Println(err)
		return nil
	}

	var ar entity.AuthorResponse
	var a entity.Author
	for authors.Next() {
		err := authors.StructScan(&a)
		if err != nil {
			log.Println(err)
			return nil
		}

		ar.Author = a

		var books []entity.Book
		query2 := `SELECT id, title, author_id FROM books WHERE author_id=$1`

		err = s.db.Select(&books, query2, a.Id)
		if err != nil {
			log.Println(err)
			return nil
		}
		ar.Books = books

		res = append(res, ar)
	}

	return res
}

func (s LibraryStorage) TopAuthors(count int) []entity.AuthorTopResponse {
	var res []entity.AuthorTopResponse

	query := `SELECT id, name, reader_count as rating FROM authors ORDER BY rating DESC LIMIT $1`

	err := s.db.Select(&res, query, count)
	if err != nil {
		log.Println(err)
		return nil
	}

	return res
}

func (s LibraryStorage) AddBook(b entity.Book) (entity.Book, error) {
	var id int64
	query := `INSERT INTO books (title, author_id) VALUES ($1, $2) RETURNING id`

	err := s.db.QueryRow(query, b.Title, b.AuthorId).Scan(&id)
	if err != nil {
		return entity.Book{}, err
	}

	b.Id = int(id)

	return b, nil
}

func (s LibraryStorage) ListBooks() []entity.BookResponse {
	var res []entity.BookResponse

	query := `SELECT id, title, author_id FROM books`

	books, err := s.db.Queryx(query)
	if err != nil {
		log.Println(err)
		return nil
	}

	var br entity.BookResponse
	var b entity.Book
	for books.Next() {
		err := books.StructScan(&b)
		if err != nil {
			log.Println(err)
			return nil
		}

		br.Book = b

		var author entity.Author
		query2 := `SELECT id, name FROM authors WHERE id=$1`

		err = s.db.Get(&author, query2, b.AuthorId)
		if err != nil {
			log.Println(err)
			return nil
		}
		br.Author = author

		res = append(res, br)
	}

	return res
}

func (s LibraryStorage) GetBook(bookId int, userId int) (entity.Book, error) {
	// check book id
	book, err := s.checkBookID(bookId)
	if err != nil {
		return entity.Book{}, err
	}

	// check user id
	_, err = s.checkUserID(userId)
	if err != nil {
		return entity.Book{}, err
	}

	// check book status
	id := s.checkBookStatus(bookId)
	if id.Valid {
		return entity.Book{}, errors.New(fmt.Sprintf("book id: %d is currently at user: %d", bookId, id.Int64))
	}

	// work
	query := `UPDATE books SET user_id=$1 WHERE id=$2`
	s.db.MustExec(query, userId, bookId)

	query = `UPDATE authors SET reader_count = reader_count + 1 WHERE id=$1`
	s.db.MustExec(query, book.AuthorId)

	return book, nil
}

func (s LibraryStorage) ReturnBook(bookId int, userId int) (entity.Book, error) {
	// check book id
	book, err := s.checkBookID(bookId)
	if err != nil {
		return entity.Book{}, err
	}

	// check user id
	_, err = s.checkUserID(userId)
	if err != nil {
		return entity.Book{}, err
	}

	// check book status
	id := s.checkBookStatus(bookId)
	if !id.Valid {
		return entity.Book{}, errors.New(fmt.Sprintf("book id: %d is currently in the library", bookId))
	}
	if id.Int64 != int64(userId) {
		return entity.Book{}, errors.New(fmt.Sprintf("user id: %d does not have this book", userId))
	}

	//work
	query := `UPDATE books SET user_id=NULL WHERE id=$1`
	s.db.MustExec(query, bookId)

	return book, nil
}

func (s LibraryStorage) checkBookID(bookId int) (entity.Book, error) {
	query := `SELECT id, title, author_id FROM books WHERE id=$1`

	var book entity.Book
	err := s.db.Get(&book, query, bookId)
	if err != nil {
		log.Println(err)
		return entity.Book{}, errors.New(fmt.Sprintf("book id: %d error: %s", bookId, err.Error()))
	}

	return book, nil
}

func (s LibraryStorage) checkBookStatus(bookId int) sql.NullInt64 {
	query := `SELECT user_id FROM books WHERE id=$1`
	var id sql.NullInt64

	_ = s.db.Get(&id, query, bookId)

	return id
}

func (s LibraryStorage) checkUserID(userId int) (entity.User, error) {
	query := `SELECT id, name FROM users WHERE id=$1`

	var user entity.User
	err := s.db.Get(&user, query, userId)
	if err != nil {
		log.Println(err)
		return entity.User{}, errors.New(fmt.Sprintf("user id: %d error: %s", userId, err.Error()))
	}

	return user, nil
}
