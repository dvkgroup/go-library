package usercase

import (
	"gitlab.com/dvkgroup/go-library/internal/entity"
)

type LibraryRepository interface {
	AddUser(u entity.User) (entity.User, error)
	ListUsers() []entity.UserResponse
	AddAuthor(a entity.Author) (entity.Author, error)
	ListAuthors() []entity.AuthorResponse
	TopAuthors(count int) []entity.AuthorTopResponse
	AddBook(b entity.Book) (entity.Book, error)
	ListBooks() []entity.BookResponse
	GetBook(bookId int, userId int) (entity.Book, error)
	ReturnBook(bookId int, userId int) (entity.Book, error)
}

type LibraryUseCase struct {
	r LibraryRepository
}

func NewHLibraryUseCase(r LibraryRepository) *LibraryUseCase {
	return &LibraryUseCase{r}
}

func (s LibraryUseCase) AddUser(u entity.User) (entity.User, error) {
	return s.r.AddUser(u)
}

func (s LibraryUseCase) ListUsers() []entity.UserResponse {
	return s.r.ListUsers()
}

func (s LibraryUseCase) AddAuthor(a entity.Author) (entity.Author, error) {
	return s.r.AddAuthor(a)
}

func (s LibraryUseCase) ListAuthors() []entity.AuthorResponse {
	return s.r.ListAuthors()
}

func (s LibraryUseCase) TopAuthors(count int) []entity.AuthorTopResponse {
	return s.r.TopAuthors(count)
}

func (s LibraryUseCase) AddBook(b entity.Book) (entity.Book, error) {
	return s.r.AddBook(b)
}

func (s LibraryUseCase) ListBooks() []entity.BookResponse {
	return s.r.ListBooks()
}

func (s LibraryUseCase) GetBook(bookId int, userId int) (entity.Book, error) {
	return s.r.GetBook(bookId, userId)
}

func (s LibraryUseCase) ReturnBook(bookId int, userId int) (entity.Book, error) {
	return s.r.ReturnBook(bookId, userId)
}
