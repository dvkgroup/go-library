package main

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
	"gitlab.com/dvkgroup/go-library/internal/api"
	"log"
)

func main() {

	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	var config map[string]string
	config, err = godotenv.Read()
	if err != nil {
		log.Fatal(err)
	}

	connStr := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		config["DB_HOST"], config["DB_PORT"], config["DB_USER"], config["DB_PASSWORD"], config["APP_NAME"])
	db, err := sqlx.Connect("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	api.Run(config, db)
}
