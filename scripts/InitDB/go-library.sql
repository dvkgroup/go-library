CREATE TABLE IF NOT EXISTS users
(
    id              SERIAL PRIMARY KEY,
    name            VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS authors
(
    id              SERIAL PRIMARY KEY,
    name            VARCHAR(255),
    reader_count    INTEGER DEFAULT 0
);

CREATE TABLE IF NOT EXISTS books
(
    id              SERIAL PRIMARY KEY,
    title           VARCHAR(255),
    author_id       INTEGER REFERENCES Authors (id),
    user_id         INTEGER NULL
);